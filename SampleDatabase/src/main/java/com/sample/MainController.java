package com.sample;

import com.sample.model.Row;
import com.sample.repository.SampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Krzysztof on 02.08.2016.
 */
@Controller
public class MainController {

    private static final String KOLUMNA_1 = "kolumna1";
    private static final String KOLUMNA_2 = "kolumna2";
    private static final String KOLUMNA_3 = "kolumna3";
    private static final String KOLUMNA_4 = "kolumna4";

    @Autowired
    private SampleRepository sampleRepository;

    @RequestMapping(value = "/" ,method = RequestMethod.GET)
    public String hello(ModelMap model){
        model.addAttribute("selection",KOLUMNA_1);
        return "hello";
    }


    @RequestMapping(value = "/search" ,method = RequestMethod.GET, params = {"column","type"})
    public String search(ModelMap model, @RequestParam("column") String column, @RequestParam("type") int type){
        List<Row> rows = new LinkedList<>();
        rows = actionResolver(type,column);
        System.out.println(rows.size());
        model.addAttribute("rows",rows);
        model.addAttribute("selection",column);
        return "hello";
    }


    private List<Row> actionResolver(int type, String column){
        if(type==1)
            return getRepetitions(column);
        else
            return getUnique(column);
    }

    private List<Row> getUnique(String column){
        switch (column) {
            case KOLUMNA_1:
                return sampleRepository.getUniqueColumnFirst();
            case KOLUMNA_2:
                return sampleRepository.getUniqueColumnSecond();
            case KOLUMNA_3:
                return sampleRepository.getUniqueColumnThird();
            case KOLUMNA_4:
                return sampleRepository.getUniqueColumnFourth();
            default:
                return sampleRepository.getUniqueColumnFirst();
        }
    }


    private List<Row> getRepetitions(String column){
        switch (column) {
            case KOLUMNA_1:
                return sampleRepository.getRepetitionsColumnFirst();
            case KOLUMNA_2:
                return sampleRepository.getRepetitionsColumnSecond();
            case KOLUMNA_3:
                return sampleRepository.getRepetitionsColumnThird();
            case KOLUMNA_4:
                return sampleRepository.getRepetitionsColumnFourth();
            default:
                return sampleRepository.getRepetitionsColumnFirst();
        }
    }

}
