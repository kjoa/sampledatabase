package com.sample.repository;

import com.sample.model.Row;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Krzysztof on 03.08.2016.
 */

@Repository
public interface SampleRepository extends CrudRepository<Row,Long> {

    /**
     *  Zdecydowałem się na stworzenie osobnych metod dla każdej kolumny,
     *  gdyż z niewiadomyh przyczyn metoda przyjmująca za parametr nazwę kolumny
     *  zawsze zwraca pustą listę
     */

    //region getUnique

    @Query(value = "SELECT * FROM tabela_testowa WHERE :column IN (SELECT :column FROM tabela_testowa GROUP BY :column HAVING count(:column)=1)", nativeQuery = true )
    List<Row> getUnique(@Param("column") String column);

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna1 IN (SELECT kolumna1 FROM tabela_testowa GROUP BY kolumna1 HAVING count(kolumna1)=1)", nativeQuery = true )
    List<Row> getUniqueColumnFirst();

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna2 IN (SELECT kolumna2 FROM tabela_testowa GROUP BY kolumna2 HAVING count(kolumna2)=1)", nativeQuery = true )
    List<Row> getUniqueColumnSecond();

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna3 IN (SELECT kolumna3 FROM tabela_testowa GROUP BY kolumna3 HAVING count(kolumna3)=1)", nativeQuery = true )
    List<Row> getUniqueColumnThird();

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna4 IN (SELECT kolumna4 FROM tabela_testowa GROUP BY kolumna4 HAVING count(kolumna4)=1)", nativeQuery = true )
    List<Row> getUniqueColumnFourth();

    //endregion

    //region getRepetitions


    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna1 IN (SELECT kolumna1 FROM tabela_testowa GROUP BY kolumna1 HAVING count(kolumna1)>1)", nativeQuery = true )
    List<Row> getRepetitionsColumnFirst();

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna2 IN (SELECT kolumna2 FROM tabela_testowa GROUP BY kolumna2 HAVING count(kolumna2)>1)", nativeQuery = true )
    List<Row> getRepetitionsColumnSecond();

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna3 IN (SELECT kolumna3 FROM tabela_testowa GROUP BY kolumna3 HAVING count(kolumna3)>1)", nativeQuery = true )
    List<Row> getRepetitionsColumnThird();

    @Query(value = "SELECT * FROM tabela_testowa WHERE kolumna4 IN (SELECT kolumna4 FROM tabela_testowa GROUP BY kolumna4 HAVING count(kolumna4)>1)", nativeQuery = true )
    List<Row> getRepetitionsColumnFourth();


    //endregion





}
