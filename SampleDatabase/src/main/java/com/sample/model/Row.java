package com.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Krzysztof on 03.08.2016.
 */

@Entity(name = "tabela_testowa")
public class Row {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "kolumna1")
    private String kolumna1;

    @Column(name = "kolumna2")
    private String kolumna2;

    @Column(name = "kolumna3")
    private String kolumna3;

    @Column(name = "kolumna4")
    private Long kolumna4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKolumna1() {
        return kolumna1;
    }

    public void setKolumna1(String kolumna1) {
        this.kolumna1 = kolumna1;
    }

    public String getKolumna2() {
        return kolumna2;
    }

    public void setKolumna2(String kolumna2) {
        this.kolumna2 = kolumna2;
    }

    public String getKolumna3() {
        return kolumna3;
    }

    public void setKolumna3(String kolumna3) {
        this.kolumna3 = kolumna3;
    }

    public Long getKolumna4() {
        return kolumna4;
    }

    public void setKolumna4(Long kolumna4) {
        this.kolumna4 = kolumna4;
    }
}
