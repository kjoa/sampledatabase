<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sample database</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="/css/custom.css" rel="stylesheet" />
</head>
<body>

    <script type="application/javascript" src="js/jquery.js"></script>
    <script type="application/javascript" src="js/bootstrap.js"></script>
    <!--script type="application/javascript" src="js/custom.js"></script-->
    <script type="text/javascript">
    function remove()
    {   alert("Hello! I am an alert box!!");

    }
    </script>
     <script>
       function assign(number)
       {
          var e = document.getElementById("columns");
          var col = e.options[e.selectedIndex].value;
          var path = 'http://localhost:8080/search?column='+col+'&type='+number;
          window.location.assign(path);
       }
      </script>


<div align="center">
	<h1 >Sample database</h1>
</div>
      <div align="center">
	  <form name="form1" method="get" >
        <select id="columns" name="columns">
        <option value="kolumna1" <c:if test="${selection == 'kolumna1'}">selected</c:if>>Kolumna1</option>
        <option value="kolumna2" <c:if test="${selection == 'kolumna2'}">selected</c:if>>Kolumna2</option>
        <option value="kolumna3" <c:if test="${selection == 'kolumna3'}">selected</c:if>>Kolumna3</option>
        <option value="kolumna4" <c:if test="${selection == 'kolumna4'}">selected</c:if>>Kolumna4</option>
        </select>
        <button type="button" onclick="javascript:assign(1);">Powtórki</button>
        <button type="button" onclick="javascript:assign(2);">Unikalne</button>
      </div>
    </form>
</p>
<p align="center">

<table align="center">
<tr>
    <th>Id</th>
    <th>Kolumna1</th>
    <th>Kolumna2</th>
    <th>Kolumna3</th>
    <th>Kolumna4</th>
</tr>
<c:forEach var="val" items="${rows}">
     <tr>
        <td>${val.id}</td>
        <td>${val.kolumna1}</td>
        <td>${val.kolumna2}</td>
        <td>${val.kolumna3}</td>
        <td>${val.kolumna4}</td>
      </tr>
</c:forEach>
</table>
</p>
</body>
</html>